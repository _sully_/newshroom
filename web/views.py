from django.shortcuts import render
from django.http import HttpResponse


def index(request):
    name = request.session.get("username", "dude")
    return HttpResponse("Hello, %s. You're at the newshroom index." % name)

